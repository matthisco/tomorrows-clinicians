import React from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableHighlight,
  Image,
  View,
  WebView
} from "react-native";
import { AndroidBackHandler } from "react-navigation-backhandler";
import { connect } from "react-redux";
import Share from "react-native-share";
import { NavigationActions, StackActions } from "react-navigation";
import StyledText from "../components/StyledText";
import VideoPlayerHeader from "../components/VideoPlayerHeader";

import VideoPlayer from "../components/VideoPlayer";
import NavigationService from "../navigation/NavigationService";
import { bookmarkVideo } from "../actions/videos";
import ImageButton from "../components/ImageButton";
class VideoPlayerScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.onShare = this.onShare.bind(this);
    this.navigateScreen = this.navigateScreen.bind(this);
    this.bookmarkVideo = this.bookmarkVideo.bind(this);
    this.loadRecapVideo = this.loadRecapVideo.bind(this);
  }

  onShare() {
    const { id } = NavigationService.getParams();
    const { videos } = this.props;
    let videoProps = videos.find(obj => obj.id == id);

    let { description, youtubeVideo, title } = videoProps;
    const url = `https://www.youtube.com/watch?v=${youtubeVideo}`;

    let shareOptions = {
      title,
      preview: "Check out this video",
      url,
      subject: "Share Link" //  for email
    };
    Share.open(shareOptions).catch(err => console.log(err));
  }

  loadRecapVideo(id) {
    const resetAction = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({
          routeName: "VideoPlayer",
          params: { id }
        })
      ]
    });
    this.props.navigation.dispatch(resetAction);
  }

  navigateScreen(id) {
    const resetAction = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({
          routeName: "Drawer",
          params: { id }
        })
      ]
    });
    const navigateAction = NavigationActions.navigate({
      routeName: "TestYourself",
      params: { id }
    });
    this.props.navigation.dispatch(resetAction);
    this.props.navigation.dispatch(navigateAction);
  }

  onBackButtonPressAndroid = () => {
    /*
     *   Returning `true` from `onBackButtonPressAndroid` denotes that we have handled the event,
     *   and react-navigation's lister will not get called, thus not popping the screen.
     *
     *   Returning `false` will cause the event to bubble up and react-navigation's listener will pop the screen.
     * */

    const { dispatch } = this.props.navigation;

    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: "Drawer" })]
    });
    this.props.navigation.dispatch(resetAction);
    return true;
  };

  bookmarkVideo() {
    const {
      bookmarkVideo,
      navigation: { navigate }
    } = this.props;
    const { id } = NavigationService.getParams();
    bookmarkVideo(id);
    navigate("Video Player");
  }

  render() {
    const { id } = NavigationService.getParams();

    const { videos } = this.props;
    let videoProps = videos.find(obj => obj.id == id);

    let { description, youtubeVideo, title, recapId, parentId } = videoProps;
    let navId = parentId || id;
    const filteredDescription = description.replace(
      /(<p[^>]+?>|<p>|<\/p>)/gim,
      ""
    );
    return (
      <AndroidBackHandler onBackPress={this.onBackButtonPressAndroid}>
        <View style={styles.container}>
          <ScrollView>
            <VideoPlayerHeader {...videoProps} onClick={this.bookmarkVideo} />
            <VideoPlayer {...videoProps} />
            <View style={styles.opacityBackground}>
              <View
                style={{
                  margin: 12
                }}
              >
                <StyledText text="Episode Overview" />
              </View>

              <Text style={[styles.description, styles.text]}>
                {filteredDescription}
              </Text>
              {recapId ? (
                <View style={styles.section}>
                  <ImageButton
                    text="Episode Recap"
                    image="recap-icon"
                    onClick={() => this.loadRecapVideo(recapId)}
                  />
                </View>
              ) : null}
              <View style={styles.section}>
                <ImageButton
                  text="Test Yourself"
                  image="test-yourself"
                  onClick={() => this.navigateScreen(navId)}
                />
              </View>

              <View style={styles.section}>
                <ImageButton
                  text="Share Video"
                  image="share-icon"
                  onClick={this.onShare}
                />
              </View>
            </View>
          </ScrollView>
        </View>
      </AndroidBackHandler>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  bookmarkVideo: id => dispatch(bookmarkVideo(id))
});

const mapStateToProps = state => {
  return {
    videos: state.tcApp.videos
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VideoPlayerScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column"
  },
  opacityBackground: {
    backgroundColor: "#00333380",
    padding: 10,
    paddingBottom: 40,
    flex: 1
  },
  text: { color: "white" },
  description: {
    fontSize: 16,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 0,
    paddingBottom: 0
  },
  section: {
    flexDirection: "row",
    borderColor: "#ffffff50",
    borderBottomWidth: 1,
    paddingBottom: 15,
    paddingTop: 10
  },
  leftContainer: {
    height: 30,
    width: 40,
    margin: 10,
    flexDirection: "row",
    justifyContent: "flex-start"
  },
  rightContainer: {
    flexDirection: "row"
  }
});
