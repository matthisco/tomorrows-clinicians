import React, { Component } from "react";

import { AppRegistry } from "react-native";
import { Provider } from "react-redux";
import App from "./App";
import { store, persistor } from "./config/configureStore";
const Root = () => (
	<Provider store={store}>
		<App />
	</Provider>
);

AppRegistry.registerComponent("tcapp", () => Root);
