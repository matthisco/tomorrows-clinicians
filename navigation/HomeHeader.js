import React, { Component } from "react";

import { connect } from "react-redux";
import {
    Image,
    View,
    Text,
    Modal,
    Button,
    TouchableOpacity,
    AsyncStorage,
    StyleSheet,
    Platform,
    Alert,
    FlatList,
    StatusBar,
    TouchableHighlight
} from "react-native";
import {
    StackActions,
    NavigationActions,
    DrawerNavigator
} from "react-navigation";
import Icon from "react-native-vector-icons/FontAwesome5";
import { ListItem, SearchBar } from "react-native-elements";
import { filteredVideo, initSearch } from "../actions/videos";

class HomeHeader extends Component {
    constructor(props) {
        super(props);
        this.state = { term: "", videos: [] };
        this.onSubmitEdit = this.onSubmitEdit.bind(this);
    }
    componentDidMount() {
        this.SearchFilterFunction("");
    }
    SearchFilterFunction(term) {
        const { filteredVideo } = this.props;
        filteredVideo(term);
    }

    onSubmitEdit() {
        const {
            navigation: { navigate },
            initSearch
        } = this.props;

        navigate("VideoEpisodes");
    }

    onPress(id) {
        const {
            navigation,
            initSearch,
            navigation: { dispatch }
        } = this.props;
        initSearch();

        const resetAction = StackActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({
                    routeName: "VideoPlayer",
                    params: { id }
                })
            ]
        });
        dispatch(resetAction);
    }
    separator = () => <View style={styles.separator} />;
    render() {
        const {
            navigation,
            videos,
            search: { term },
            scene: {
                route: { routeName: title }
            }
        } = this.props;
        return (
            <View>
                <View style={styles.container}>
                    <Image
                        source={require("../assets/images/tc-logo.png")}
                        style={{ width: 120, height: 40 }}
                    />

                    <Image
                        source={require("../assets/images/connect-logo.png")}
                        style={{ width: 50, height: 60 }}
                    />
                </View>
                <SearchBar
                    round
                    containerStyle={{
                        borderTopWidth: 0,
                        borderBottomWidth: 0,
                        backgroundColor: "transparent"
                    }}
                    inputStyle={{ backgroundColor: "white" }}
                    inputContainerStyle={{
                        borderRadius: 5,
                        backgroundColor: "white"
                    }}
                    icon={{
                        type: "fontAwesome",
                        color: "#86939e",
                        name: "fa-search"
                    }}
                    onSubmitEditing={this.onSubmitEdit}
                    searchIcon={{ size: 18 }}
                    onChangeText={text => this.SearchFilterFunction(text)}
                    onClear={text => this.SearchFilterFunction("")}
                    placeholder="Type Here..."
                    value={term}
                />

                <FlatList
                    data={this.props.search.videos}
                    renderItem={({ item }) => (
                        <Text
                            onPress={() => this.onPress(item.id)}
                            style={{
                                backgroundColor: "#00336680",
                                color: "white",
                                padding: 10
                            }}
                        >
                            {item.title}
                        </Text>
                    )}
                    contentContainerStyle={{
                        overflow: "hidden"
                    }}
                    keyExtractor={item => item.id.toString()}
                    ItemSeparatorComponent={this.separator}
                />
            </View>
        );
    }
}
const mapDispatchToProps = dispatch => ({
    filteredVideo: data => dispatch(filteredVideo(data)),
    initSearch: () => dispatch(initSearch())
});

const mapStateToProps = state => {
    return {
        videos: state.tcApp.videos,
        search: state.tcApp.search
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HomeHeader);

const styles = StyleSheet.create({
    container: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        padding: 10,
        marginTop: Platform.OS === "ios" ? 20 : 0,
        paddingTop: Platform.OS === "ios" ? 20 : 10
    },
    title: {
        color: "#fff",
        fontSize: 18
    },
    separator: {
        borderBottomColor: "#d1d0d4",
        borderBottomWidth: 1
    }
});
