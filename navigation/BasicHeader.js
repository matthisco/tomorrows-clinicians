import React, { Component } from "react";

import { connect } from "react-redux";
import {
    Image,
    View,
    Text,
    Modal,
    Button,
    TouchableOpacity,
    AsyncStorage,
    StyleSheet,
    Platform,
    Alert,
    StatusBar,
    TouchableHighlight
} from "react-native";
import {
    StackActions,
    NavigationActions,
    DrawerNavigator
} from "react-navigation";
import Icon from "react-native-vector-icons/FontAwesome5";
import StyledText from "../components/StyledText";
class BasicHeader extends Component {
    constructor() {
        super();
        this.resetForm = this.resetForm.bind(this);
    }
    resetForm() {
        const { goBack } = this.props;

        const resetAction = StackActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({
                    routeName: "Drawer"
                })
            ]
        });

        const navigateAction = NavigationActions.navigate({
            routeName: "VideoEpisodes"
        });

        this.props.navigation.dispatch(resetAction);
        this.props.navigation.dispatch(navigateAction);
    }

    render() {
        const {
            initialRoute,
            scene: {
                route: { routeName }
            }
        } = this.props;

        return (
            <View style={styles.container}>
                <View>
                    <TouchableHighlight
                        style={{
                            width: 32,
                            marginLeft: 10,
                            marginTop: 10,
                            marginRight: 5,
                            alignSelf: "flex-start"
                        }}
                        onPress={this.resetForm}
                    >
                        <Icon name="chevron-left" size={20} color="#fff" />
                    </TouchableHighlight>
                    <View style={{ paddingLeft: 5 }}>
                        <StyledText text={title} />
                    </View>
                </View>
                <View>
                    <Image
                        source={require("../assets/images/tc-logo.png")}
                        style={{
                            width: 120,
                            height: 40,
                            marginRight: 5,
                            padding: 10
                        }}
                    />

                    <TouchableOpacity
                        onPress={() => {
                            this.props.navigation.openDrawer();
                        }}
                        style={{
                            margin: 5,
                            alignSelf: "flex-end"
                        }}
                    >
                        <Icon name="bars" size={20} color="#fff" />
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

export default BasicHeader;

const styles = StyleSheet.create({
    container: {
        backgroundColor: "transparent",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: Platform.OS === "ios" ? 50 : 10,

        minHeight: 50,
        position: "relative"
    },
    title: {
        color: "#fff",
        fontSize: 18
    },
    text: {
        color: "#fff",
        fontSize: 14
    }
});
