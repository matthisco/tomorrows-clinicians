import React, { Component } from "react";
import {
	Image,
	View,
	Text,
	Modal,
	Button,
	TouchableOpacity,
	AsyncStorage,
	StyleSheet,
	Platform,
	Alert,
	Dimensions,
	TouchableHighlight
} from "react-native";
import {
	createDrawerNavigator,
	createStackNavigator,
	createAppContainer
} from "react-navigation";

import HomeScreen from "../screens/HomeScreen";
import AboutScreen from "../screens/AboutScreen";
import MyResultsScreen from "../screens/MyResultsScreen";
import TestsScreen from "../screens/TestsScreen";
import BookmarkedVideosScreen from "../screens/BookmarkedVideosScreen";
import TestYourselfScreen from "../screens/TestYourselfScreen";
import VideoEpisodesScreen from "../screens/VideoEpisodesScreen";
import VideoPlayerScreen from "../screens/VideoPlayerScreen";
import SearchScreen from "../screens/SearchScreen";
import VideoHeader from "./VideoHeader";
import DrawerHeader from "./DrawerHeader";
import HomeHeader from "./HomeHeader";
import SideMenu from "./SideMenu";
const config = {
	contentOptions: {
		activeTintColor: "#e91e63",
		inactiveTintColor: "#ffffff",
		itemStyle: {
			flexDirection: "row-reverse"
		}
	},
	drawerWidth: Dimensions.get("window").width,
	drawerPosition: "left",
	contentComponent: SideMenu,
	drawerBackgroundColor: "#00000020",
	transparentCard: true,
	cardStyle: {
		backgroundColor: "transparent",
		opacity: 1
	},
	transitionConfig: () => ({
		containerStyle: {
			backgroundColor: "transparent"
		}
	})
};

const withHeader = (
	screen: Function,
	routeName: string,
	Header
): StackNavigator =>
	createStackNavigator(
		{
			[routeName]: {
				screen,
				navigationOptions: ({ routeName, props }) => ({
					header: props => <Header {...props} />
				})
			}
		},
		{
			initialRoute: "Home",
			transparentCard: true,
			cardStyle: {
				backgroundColor: "transparent",
				opacity: 1
			},
			transitionConfig: () => ({
				containerStyle: {
					backgroundColor: "transparent"
				}
			})
		}
	);

const routes = {
	VideoEpisodes: {
		screen: withHeader(VideoEpisodesScreen, "Video Episodes", DrawerHeader)
	},
	TestYourself: {
		screen: withHeader(TestYourselfScreen, "Test Yourself", DrawerHeader),
		navigationOptions: {
			drawerLabel: () => null
		}
	},
	MyResults: {
		screen: withHeader(MyResultsScreen, "My Results", DrawerHeader)
	},

	About: {
		screen: withHeader(AboutScreen, "About", DrawerHeader)
	},
	Tests: {
		screen: withHeader(TestsScreen, "Test Yourself", DrawerHeader)
	},
	BookmarkedVideos: {
		screen: withHeader(
			BookmarkedVideosScreen,
			"Bookmarked Videos",
			DrawerHeader
		)
	}
};

const NestedDrawer = createDrawerNavigator(routes, config);

const MainStack = createStackNavigator(
	{
		Home: {
			screen: HomeScreen,
			navigationOptions: ({ props }) => ({
				header: props => <HomeHeader {...props} />
			})
		},
		Drawer: {
			screen: NestedDrawer,
			navigationOptions: ({ props }) => ({
				header: () => null
			})
		},
		VideoPlayer: {
			screen: VideoPlayerScreen,
			navigationOptions: ({ props }) => ({
				header: props => <VideoHeader {...props} />
			})
		}
	},
	{
		initialRoute: "Home",
		transparentCard: true,
		cardStyle: {
			backgroundColor: "transparent",
			opacity: 1
		},
		transitionConfig: () => ({
			containerStyle: {
				backgroundColor: "transparent"
			}
		})
	}
);

export default createAppContainer(MainStack);
