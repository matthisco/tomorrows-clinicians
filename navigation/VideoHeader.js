import React, { Component } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    TouchableHighlight,
    Image,
    FlatList,
    StyleSheet,
    StatusBar,
    Platform
} from "react-native";

import Icon from "react-native-vector-icons/FontAwesome5";
import { connect } from "react-redux";
import {
    StackActions,
    NavigationActions,
    DrawerNavigator
} from "react-navigation";
import { ListItem, SearchBar } from "react-native-elements";
import { filteredVideo, initSearch } from "../actions/videos";
import StyledText from "../components/StyledText";
class DrawerHeader extends Component {
    constructor(props) {
        super(props);
        this.state = { term: "", videos: [] };
        this.onSubmitEdit = this.onSubmitEdit.bind(this);
        this.resetForm = this.resetForm.bind(this);
    }
    componentDidMount() {
        this.SearchFilterFunction("");
    }
    SearchFilterFunction(term) {
        const { filteredVideo } = this.props;
        filteredVideo(term);
    }

    onSubmitEdit() {
        const {
            navigation: { navigate },
            initSearch
        } = this.props;

        navigate("VideoEpisodes");
    }

    onPress(id) {
        const {
            navigation,
            initSearch,
            navigation: { dispatch }
        } = this.props;
        initSearch();

        const resetAction = StackActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({
                    routeName: "VideoPlayer",
                    params: { id }
                })
            ]
        });
        dispatch(resetAction);
    }
    resetForm() {
        const { dispatch, goBack } = this.props.navigation;

        const resetAction = StackActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({
                    routeName: "Drawer"
                })
            ]
        });
        this.props.navigation.dispatch(resetAction);
        this.props.navigation.navigate("VideoEpisodes");
    }
    render() {
        const {
            navigation,
            videos,
            search: { term },
            scene: {
                route: { routeName: title }
            }
        } = this.props;

        return (
            <View>
                <View style={styles.container}>
                    <View>
                        <TouchableOpacity
                            style={{
                                width: 32,
                                marginLeft: 10,
                                marginTop: 10,
                                marginRight: 5,
                                alignSelf: "flex-start"
                            }}
                            onPress={this.resetForm}
                        >
                            <Icon name="chevron-left" size={20} color="#fff" />
                        </TouchableOpacity>
                    </View>
                    <View>
                        <Image
                            source={require("../assets/images/tc-logo.png")}
                            style={{
                                width: 120,
                                height: 40,
                                marginRight: 5,
                                padding: 10
                            }}
                        />
                    </View>
                </View>
            </View>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    filteredVideo: data => dispatch(filteredVideo(data)),
    initSearch: () => dispatch(initSearch())
});

const mapStateToProps = state => {
    return {
        videos: state.tcApp.videos,
        search: state.tcApp.search
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DrawerHeader);

const styles = StyleSheet.create({
    container: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        paddingRight: 10,
        marginBottom: 10,
        marginTop: Platform.OS === "ios" ? 20 : 0,
        paddingTop: Platform.OS === "ios" ? 20 : 10
    },
    title: {
        color: "#fff",
        fontSize: 18
    }
});
