import React, { Component } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    TouchableHighlight,
    Image,
    FlatList,
    StyleSheet,
    StatusBar,
    Platform
} from "react-native";

import Icon from "react-native-vector-icons/FontAwesome5";
import { connect } from "react-redux";
import {
    StackActions,
    NavigationActions,
    DrawerNavigator
} from "react-navigation";
import { ListItem, SearchBar } from "react-native-elements";
import { filteredVideo, initSearch } from "../actions/videos";
import StyledText from "../components/StyledText";
class TestHeader extends Component {
    constructor(props) {
        super(props);
        this.state = { term: "", videos: [] };
        this.onSubmitEdit = this.onSubmitEdit.bind(this);
        this.resetForm = this.resetForm.bind(this);
    }
    componentDidMount() {
        this.SearchFilterFunction("");
    }
    SearchFilterFunction(term) {
        const { filteredVideo } = this.props;
        filteredVideo(term);
    }

    onSubmitEdit() {
        const {
            navigation: { navigate },
            initSearch
        } = this.props;

        navigate("VideoEpisodes");
    }

    onPress(id) {
        const {
            navigation,
            initSearch,
            navigation: { dispatch }
        } = this.props;
        initSearch();

        const resetAction = StackActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({
                    routeName: "VideoPlayer",
                    params: { id }
                })
            ]
        });
        dispatch(resetAction);
    }
    resetForm() {
        const { dispatch, goBack } = this.props.navigation;

        const resetAction = StackActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({
                    routeName: "Home"
                })
            ]
        });
        this.props.navigation.dispatch(resetAction);
        this.props.navigation.navigate("Home");
    }
    render() {
        const {
            navigation,
            videos,
            search: { term },
            scene: {
                route: { routeName: title }
            }
        } = this.props;
        return (
            <View>
    <Text>Hi</Text>
            </View>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    filteredVideo: data => dispatch(filteredVideo(data)),
    initSearch: () => dispatch(initSearch())
});

const mapStateToProps = state => {
    return {
        videos: state.tcApp.videos,
        search: state.tcApp.search
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(TestHeader);

const styles = StyleSheet.create({
    container: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        paddingRight: 10,
        marginTop: Platform.OS === "ios" ? 20 : 0,
        paddingTop: Platform.OS === "ios" ? 20 : 10
    },
    title: {
        color: "#fff",
        fontSize: 18
    }
});
