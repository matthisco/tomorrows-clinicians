import React, { Component } from "react";
import {
	Image,
	View,
	Text,
	Modal,
	Button,
	TouchableOpacity,
	AsyncStorage,
	StyleSheet,
	Platform,
	Alert,
	TouchableHighlight
} from "react-native";
import {
	createDrawerNavigator,
	createStackNavigator,
	createAppContainer
} from "react-navigation";
import HomeScreen from "../screens/HomeScreen";
import LinksScreen from "../screens/LinksScreen";
import SettingsScreen from "../screens/SettingsScreen";
import AboutScreen from "../screens/AboutScreen";
import MyResultsScreen from "../screens/MyResultsScreen";
import SearchScreen from "../screens/SearchScreen";
import BookmarkedVideosScreen from "../screens/BookmarkedVideosScreen";
import TestYourselfScreen from "../screens/TestYourselfScreen";
import VideoEpisodesScreen from "../screens/VideoEpisodesScreen";
import VideoPlayerScreen from "../screens/VideoPlayerScreen";
import BasicHeader from "./BasicHeader";
import DrawerHeader from "./DrawerHeader";

const config = {
	initialRouteName: "Home",
	contentOptions: {
		activeTintColor: "#e91e63",
		itemsContainerStyle: {
			// opacity: 1
		},
		iconContainerStyle: {
			// opacity: 1
		},
		itemStyle: {
			flexDirection: "row-reverse"
		}
	},
	drawerWidth: 300,
	drawerPosition: "right"
};

function forVertical(props) {
	const { layout, position, scene } = props;

	const index = scene.index;
	const height = layout.initHeight;

	const translateX = 0;
	const translateY = position.interpolate({
		inputRange: ([index - 1, index, index + 1]: Array<number>),
		outputRange: ([height, 0, 0]: Array<number>)
	});

	return {
		transform: [{ translateX }, { translateY }]
	};
}

const withHeader = (
	screen: Function,
	routeName: string,
	Header
): StackNavigator =>
	createStackNavigator(
		{
			[routeName]: {
				screen,
				navigationOptions: ({ navigation, routeName, props }) => ({
					header: props => <Header {...props} />
				})
			}
		},
		{
			transparentCard: true,
			transitionConfig: () => ({ screenInterpolator: forVertical })
		}
	);

const routes = {
	Home: {
		screen: withHeader(HomeScreen, "Home", BasicHeader)
	},
	Links: {
		screen: withHeader(LinksScreen, "Links", DrawerHeader)
	},
	Settings: {
		screen: withHeader(SettingsScreen, "Settings", DrawerHeader)
	},
	VideoEpisodes: {
		screen: withHeader(VideoEpisodesScreen, "Video Episodes", DrawerHeader)
	},
	VideoPlayer: {
		screen: withHeader(VideoPlayerScreen, "Video Player", DrawerHeader)
	},
	TestYourself: {
		screen: withHeader(TestYourselfScreen, "Test Yourself", DrawerHeader)
	},
	MyResults: {
		screen: withHeader(MyResultsScreen, "My Results", DrawerHeader)
	},
	BookmarkedVideos: {
		screen: withHeader(
			BookmarkedVideosScreen,
			"Bookmarked Videos",
			DrawerHeader
		)
	},
	Search: {
		screen: withHeader(SearchScreen, "Search", DrawerHeader)
	},
	About: {
		screen: withHeader(AboutScreen, "About", DrawerHeader)
	}
};

const AppNavigator = createDrawerNavigator(routes, config);

export default createAppContainer(AppNavigator);
