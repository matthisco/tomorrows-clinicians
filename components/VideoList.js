import React, { Component } from "react";
import { View, Text, Image, StyleSheet, Platform } from "react-native";

import VideoEpisode from "./VideoEpisode";

import { images } from "../helpers/images";
export default class VideoList extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { videos } = this.props;

    return videos.map((item, key) => (
      <VideoEpisode
        icon={item.icon}
        key={key}
        id={item.id}
        description={item.description}
        preview={item.preview}
        title={item.title}
        video={item.youtubeVideo}
        {...this.props}
      />
    ));
  }
}
const styles = StyleSheet.create({});
