import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  StyleSheet,
  Platform,
  TouchableHighlight
} from "react-native";
import { StackActions, NavigationActions } from "react-navigation";
import CustomImage from "./CustomImage";

export default class VideoEpisode extends React.Component {
  constructor(props) {
    super(props);
    this.state = { height: 215 };
  }

  render() {
    const {
      id,
      title,
      description,
      video,
      preview,
      push,
      dispatch,
      testLink
    } = this.props;

    return (
      <TouchableHighlight
        style={styles.episode}
        activeOpacity={1}
        underlayColor="#808080"
        onPress={() => {
          const resetAction = StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                routeName: testLink ? "TestYourself" : "VideoPlayer",
                params: { id }
              })
            ]
          });
          dispatch(resetAction);
        }}
      >
        <View style={styles.title}>
          <Text style={styles.text}>{title}</Text>
        </View>
      </TouchableHighlight>
    );
  }
}
const styles = StyleSheet.create({
  episode: {
    width: "100%",
    height: 40,
    margin: 1,
    opacity: 0.7,
    marginTop: 2,
    padding: 5,
    paddingLeft: 10,
    justifyContent: "flex-start",
    backgroundColor: "#003366"
  },
  icon: {},
  iconStyle: { width: 80, height: 80, paddingTop: 10 },
  title: {
    color: "white",
    flex: 1,
    flexWrap: "wrap"
  },
  content: {
    alignItems: "center"
  },
  text: {
    color: "white",
    fontSize: 16,
    justifyContent: "flex-start",
    marginTop: 5,
    fontFamily: "Lato-Regular"
  }
});
