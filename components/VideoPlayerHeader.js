import React, { Component } from "react";
import PropTypes from "prop-types";

import {
	StyleSheet,
	Text,
	View,
	Dimensions,
	Image,
	TouchableHighlight
} from "react-native";
import StyledText from "./StyledText";
import { images } from "../helpers/images";
export default class VideoPlayerHeader extends React.Component {
	constructor(props) {
		super(props);
		this.handleClick = this.handleClick.bind(this);
	}

	handleClick() {
		let { onClick, id } = this.props;

		onClick(id);
	}

	render() {
		let { title, bookMarked, icon, id } = this.props;

		let imageSource = images[icon].uri;
		return (
			<View style={styles.container}>
				<View style={styles.navBar}>
					<View style={styles.leftContainer}>
						<Image
							style={styles.iconStyle}
							resizeMode="contain"
							source={imageSource}
						/>
					</View>
					<View style={styles.title}>
						<StyledText text={title} style={{fontSize: 16
						}} />
					</View>
					<View style={styles.rightContainer}>
						<TouchableHighlight onPress={this.handleClick}>
							{bookMarked ? (
								<Image
									style={{
										width: 25,
										height: 32
									}}
									source={require("../assets/images/bookmark-filled.png")}
								/>
							) : (
								<Image
									style={{
										width: 25,
										height: 32
									}}
									source={require("../assets/images/bookmark.png")}
								/>
							)}
						</TouchableHighlight>
					</View>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: { height: 70, width: "100%" },
	title: {
		marginLeft: 10,
		width: Dimensions.get('window').width - 90,
		flexWrap: "wrap"
	},
	iconStyle: { width: 40, height: 40 },
	navBar: {
		width: "100%",
		padding: 5,
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		borderTopWidth: 1,
		borderTopColor: "#ffffff50",
		borderBottomWidth: 1,
		borderBottomColor: "#ffffff50"
	},
	leftContainer: {
		height: 40,
		width: 30,
		flexDirection: "row",
		justifyContent: "flex-start",
		marginLeft: 10,
		marginRight: 10
	},
	rightContainer: {
		flexDirection: "row",
		width: 30,
		justifyContent: "flex-end",
		alignItems: "center",
		paddingRight: 10
	}
});
