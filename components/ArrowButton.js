import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Clipboard,
  ToastAndroid,
  AlertIOS,
  Image,
  Platform,
  Linking
} from "react-native";

import StyledText from "./StyledText";
import Icon from "react-native-vector-icons/FontAwesome5";
export default class ImageButton extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    let { text, bottom, link } = this.props;

    console.log(text, bottom, link);

    return (
      <TouchableOpacity
        style={styles.container}
        onPress={() => {
          Linking.openURL(link);
        }}
      >
        <View style={styles.textContainer}>
          <StyledText text={text} />
        </View>
        <View style={styles.imageContainer}>
          <Icon name="chevron-right" size={20} color="#fff" />
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: "90%",
    paddingTop: 10,
    paddingBottom: 10,
    marginLeft: 25,
    flexDirection: "row",
    borderRadius: 4,
    borderTopWidth: typeof bottom === "undefined" ? 2 : 0,
    borderBottomWidth: typeof bottom === "undefined" ? 0 : 2,


    
    borderColor: "#d6d7da"
  },
  imageContainer: {
    flex: 2,
    flexDirection: "row",
    justifyContent: "flex-end",
  },
  textContainer: {
    flex: 10,
    flexDirection: "row"
  }
});
